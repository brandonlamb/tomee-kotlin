package net.brandonlamb.api.common;

import javax.ws.rs.core.Response;

@FunctionalInterface
public interface InvocationHandler {

  void invoke(Response response);
}
