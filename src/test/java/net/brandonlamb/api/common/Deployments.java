package net.brandonlamb.api.common;

import org.apache.ziplock.maven.Mvn;
import org.jboss.shrinkwrap.api.spec.WebArchive;

import java.io.File;

public class Deployments {

  private static WebArchive war;

  public static WebArchive war() {
    System.out.println("\n= = = = =\nDeploy WAR\n= = = = =\n");

    if (war == null) {
      war = new Mvn.Builder()
          .build(WebArchive.class)
          .addPackages(true, "com.sbux.gred.mock")
          .addPackages(true, "com.sbux.gred.example")
          .addAsWebInfResource(
              new File("src/main/webapp/WEB-INF/openejb-jar.xml"),
              "openejb-jar.xml"
          )
          .addAsWebInfResource(
              new File("src/main/webapp/WEB-INF/resources.xml"),
              "resources.xml"
          )
          .addAsWebInfResource(
              new File("src/test/resources/beans.xml"),
              "beans.xml"
          )
          .addAsWebInfResource(
              new File("src/main/webapp/WEB-INF/ejb-jar.xml"),
              "ejb-jar.xml"
          )
      ;
    }

    return war;
  }
}
