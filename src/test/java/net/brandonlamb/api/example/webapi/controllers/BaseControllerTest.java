package net.brandonlamb.api.example.webapi.controllers;

import net.brandonlamb.api.common.Deployments;
import net.brandonlamb.api.common.InvocationHandler;

import org.apache.johnzon.jaxrs.JohnzonProvider;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.spec.WebArchive;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class BaseControllerTest {

  private final static String LANGUAGE = Locale.ENGLISH.toString();
  private final static List<MediaType> mediaTypes =
      Arrays.asList(MediaType.APPLICATION_XML_TYPE, MediaType.APPLICATION_JSON_TYPE);

  @ArquillianResource
  protected URL baseURL;
  protected Client client;

  @Deployment
  public static WebArchive createDeployment() {
    return Deployments.war();
  }

  protected Client createClient() {
    if (client == null) {
      client = ClientBuilder
          .newClient()
          .register(JohnzonProvider.class)
//          .register(JacksonFeature.class)
//          .register(ClientResponseLoggingFilter.class)
//          .register(new LoggingFeature())
      ;
    }
    return client;
  }

  protected void execute(final WebTarget target, final InvocationHandler handler) {
    mediaTypes.forEach(i -> handler.invoke(
        target
            .request(i)
            .acceptLanguage(LANGUAGE)
            .buildGet()
            .invoke())
    );
  }

  protected void doDelete(final WebTarget target, final InvocationHandler handler) {
    mediaTypes.forEach(i -> handler.invoke(
        target
            .request()
            .acceptLanguage(LANGUAGE)
            .buildDelete()
            .invoke())
    );
  }
//
//  protected HttpClientWrapper createHttpClient() {
//    final Config config = Config.builder()
//        .language(LANGUAGE)
//        .version(1)
//        .scheme("http")
//        .host(baseURL.getHost())
//        .build();
//
//    return new HttpClientWrapper(config, false);
//  }
}
