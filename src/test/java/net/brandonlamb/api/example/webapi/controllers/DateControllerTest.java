package net.brandonlamb.api.example.webapi.controllers;

import net.brandonlamb.api.example.webapi.models.Calendar;

import junit.framework.TestCase;

import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.WebTarget;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

@RunWith(Arquillian.class)
public class DateControllerTest extends BaseControllerTest {

  private final String CALENDAR_PATH = "/2016-04-30";

  @RunAsClient
  @Test
  public void validRequest() {
    final WebTarget target = createClient()
        .target(baseURL.toExternalForm())
        .path(CALENDAR_PATH);
    execute(target, res -> {
      final int status = res.getStatus();
      System.out.println("STATUS: " + status);
      Assert.assertEquals(200, res.getStatus());
      final Calendar entity = res.readEntity(Calendar.class);
      TestCase.assertNotNull(entity);
      {
        Assert.assertEquals(8, entity.getFiscalPeriod());
        Assert.assertEquals(2016, entity.getFiscalYear());
        Assert.assertEquals("2016-04-25", entity.getFiscalPeriodBeginDate());
        Assert.assertEquals("2016-05-22", entity.getFiscalPeriodEndDate());
      }
    });
  }
}
