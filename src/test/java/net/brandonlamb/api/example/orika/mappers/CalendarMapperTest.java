package net.brandonlamb.api.example.orika.mappers;

import net.brandonlamb.api.example.mock.dal.CalendarFactory;
import net.brandonlamb.api.example.webapi.mapper.orika.CalendarMapper;
import net.brandonlamb.api.example.webapi.models.Calendars;
import net.brandonlamb.api.common.Deployments;
import com.sbux.gred.common.models.Models;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
public class CalendarMapperTest {

  @Inject
  private CalendarMapper mapper;

  @Deployment
  public static WebArchive createDeployment() {
    return Deployments.war();
  }

  @Test
  public void testInstanceMapper() {
    final Calendar dalModel = CalendarFactory
        .createCalendar()
        .stream()
        .findAny()
        .get();
    final Calendar apiModel = mapper.map(dalModel);
    testMappedObject(apiModel, dalModel);
  }

  @Test
  public void testListMapper() {
    final List<Calendar> dalModels = CalendarFactory
        .createCalendar()
        .stream()
        .limit(3)
        .collect(Collectors.toList());
    final Models<Calendar> models = new Models<>(dalModels, dalModels.size());
    final Calendars calendars = mapper.map(models);
    IntStream.range(0, dalModels.size())
        .forEach(i -> testMappedObject(calendars.getCalendars().get(i), dalModels.get(i)));
  }

  private void testMappedObject(Calendar apiModel, Calendar dalModel) {
    assertNotNull(apiModel);
    assertNotNull(dalModel);
  }
}
