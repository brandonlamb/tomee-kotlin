package net.brandonlamb.api.example.mock.dal;

import net.brandonlamb.api.example.dal.common.models.Calendar;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CalendarFactory {

  public static List<Calendar> createCalendar() {
    final List<Calendar> calendar = new ArrayList<>();

    calendar.add(
        Calendar.builder()
            .calendarDate(LocalDate.of(2016, 04, 30))
            .calendarDayOfWeek(6)
            .calendarDayOfWeekName("Saturday")
            .calendarDayOfYear(121)
            .calendarMonthName("April")
            .fiscalDayOfWeek(6)
            .fiscalDayOfYear(2016)
            .fiscalDayPeriod(8)
            .fiscalPeriodBeginDate(LocalDate.of(2016, 04, 25))
            .fiscalPeriodDays(28)
            .fiscalPeriodEndDate(LocalDate.of(2016, 05, 22))
            .fiscalPeriodWeeks(4)
            .fiscalWeek(31)
            .fiscalWeekBeginDate(LocalDate.of(2016, 04, 25))
            .fiscalYear(2016)
            .fiscalWeekBeginDate(LocalDate.of(2016, 04, 25))
            .fiscalWeekEndDate(LocalDate.of(2016, 05, 01))
            .fiscalYear(2016)
            .fiscalYearEndDate(LocalDate.of(2016, 10, 02))
            .fiscalYearWeeks(53)
            .build()
    );

    calendar.add(
        Calendar.builder()
            .calendarDate(LocalDate.of(2016, 05, 05))
            .calendarDayOfWeek(4)
            .calendarDayOfWeekName("Thursday")
            .calendarDayOfYear(126)
            .calendarMonthName("May")
            .fiscalDayOfWeek(4)
            .fiscalDayOfYear(221)
            .fiscalDayPeriod(11)
            .fiscalMonthName("May")
            .fiscalPeriodBeginDate(LocalDate.of(2016, 04, 25))
            .fiscalPeriodDays(28)
            .fiscalPeriodEndDate(LocalDate.of(2016, 05, 22))
            .fiscalPeriodWeeks(4)
            .fiscalWeek(32)
            .fiscalYear(2016)
            .fiscalWeekBeginDate(LocalDate.of(2016, 05, 02))
            .fiscalWeekEndDate(LocalDate.of(2016, 05, 04))
            .fiscalYear(2016)
            .fiscalYearEndDate(LocalDate.of(2016, 10, 02))
            .fiscalYearWeeks(53)
            .build()
    );

    calendar.add(
        Calendar.builder()
            .calendarDate(LocalDate.of(2016, 06, 05))
            .calendarDayOfWeek(7)
            .calendarDayOfWeekName("Sunday")
            .calendarDayOfYear(157)
            .calendarMonthName("June")
            .fiscalDayOfWeek(7)
            .fiscalDayOfYear(252)
            .fiscalDayPeriod(14)
            .fiscalPeriodBeginDate(LocalDate.of(2016, 05, 23))
            .fiscalPeriodDays(35)
            .fiscalPeriodEndDate(LocalDate.of(2016, 06, 26))
            .fiscalPeriodWeeks(5)
            .fiscalWeek(36)
            .fiscalWeekBeginDate(LocalDate.of(2016, 05, 30))
            .fiscalYear(2016)
            .fiscalWeekBeginDate(LocalDate.of(2016, 05, 30))
            .fiscalWeekEndDate(LocalDate.of(2016, 06, 05))
            .fiscalYear(2016)
            .fiscalYearEndDate(LocalDate.of(2016, 10, 02))
            .fiscalYearWeeks(53)
            .build()
    );
    return calendar;
  }
}
