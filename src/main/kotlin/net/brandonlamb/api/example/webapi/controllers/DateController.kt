package net.brandonlamb.api.example.webapi.controllers

import net.brandonlamb.api.common.converter.DateConverter
import net.brandonlamb.api.common.webapi.pagination.PagedRequest
import net.brandonlamb.api.example.provider.CalendarProvider
import net.brandonlamb.api.example.webapi.mapper.orika.CalendarMapper
import javax.ejb.Lock
import javax.ejb.LockType.READ
import javax.ejb.Singleton
import javax.inject.Inject
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.MediaType.APPLICATION_XML
import javax.ws.rs.core.Response

@Singleton
@Lock(READ)
@Consumes(APPLICATION_JSON, APPLICATION_XML)
@Produces(APPLICATION_JSON, APPLICATION_XML)
open class DateController @Inject constructor(
  var calendarMapper: CalendarMapper,
  var calendarProvider: CalendarProvider,
  var pagedRequest: PagedRequest
) : BaseController() {
  @GET
  @Path("/{calendarDate}")
  open fun getCalendarDate(
    @PathParam("calendarDate") calendarDate: String?
  ): Response = Response.ok(calendarMapper.map(calendarProvider.findByCalendarDate(
    DateConverter.toLocalDate(calendarDate, 212001)!!, locale
  ))).build()
}
