package net.brandonlamb.api.example.webapi.models

import javax.xml.bind.annotation.XmlAccessOrder
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorOrder
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "example")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlAccessorOrder(XmlAccessOrder.ALPHABETICAL)
data class Calendar(
  var calendarDate: String = "",
  var calendarDayOfWeek: Int = 0,
  var calendarDayOfYear: Int = 0,
  var calendarDayOfWeekName: String = "",
  var calendarMonthName: String = "",
  var fiscalMonthName: String = "",
  var fiscalYear: Int = 0,
  var fiscalYearBeginDate: String = "",
  var fiscalYearEndDate: String = "",
  var fiscalYearWeeks: Int = 0,
  var fiscalPeriod: Int = 0,
  var fiscalPeriodBeginDate: String = "",
  var fiscalPeriodEndDate: String = "",
  var fiscalPeriodWeeks: Int = 0,
  var fiscalPeriodDays: Int = 0,
  var fiscalWeek: Int = 0,
  var fiscalWeekBeginDate: String = "",
  var fiscalWeekEndDate: String = "",
  var fiscalDayOfWeek: Int = 0,
  var fiscalDayOfYear: Int = 0,
  var fiscalDayPeriod: Int = 0
)
