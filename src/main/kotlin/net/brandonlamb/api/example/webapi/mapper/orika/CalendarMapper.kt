package net.brandonlamb.api.example.webapi.mapper.orika

import ma.glasnost.orika.MapperFactory
import net.brandonlamb.api.common.models.Models
import net.brandonlamb.api.common.webapi.models.Paging
import net.brandonlamb.api.example.webapi.models.Calendars
import javax.ejb.Lock
import javax.ejb.LockType.READ
import javax.ejb.Singleton
import javax.inject.Inject
import net.brandonlamb.api.example.dal.common.models.Calendar as DalModel
import net.brandonlamb.api.example.webapi.models.Calendar as ApiModel

@Singleton
@Lock(READ)
open class CalendarMapper @Inject constructor(var mapperFactory: MapperFactory) {
  /**
   * Map Models<DalModel> to webapi model
   *
   * @param dalModels
   * @return Calendars
   */
  open fun map(dalModels: Models<net.brandonlamb.api.example.dal.common.models.Calendar>): Calendars {
    val apiModels = Calendars(
      Paging(),
      mapperFactory.mapperFacade.mapAsList(dalModels.models, net.brandonlamb.api.example.webapi.models.Calendar::class.java)
    )

    val paging = apiModels.paging
    paging.total = dalModels.total
    paging.returned = dalModels.models.size

    return apiModels
  }

  /**
   * Map a DAL model to a webapi model

   * @param dalModel DAL model
   * *
   * @return webapi model
   */
  open fun map(dalModel: net.brandonlamb.api.example.dal.common.models.Calendar): net.brandonlamb.api.example.webapi.models.Calendar = mapperFactory.mapperFacade.map(dalModel, net.brandonlamb.api.example.webapi.models.Calendar::class.java)
}
