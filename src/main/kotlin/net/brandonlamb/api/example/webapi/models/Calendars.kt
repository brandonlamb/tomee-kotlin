package net.brandonlamb.api.example.webapi.models

import net.brandonlamb.api.common.webapi.models.Paging
import javax.xml.bind.annotation.XmlAccessOrder
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorOrder
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlElementWrapper
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlAccessorOrder(XmlAccessOrder.ALPHABETICAL)
class Calendars {
  var paging: Paging = Paging()
    private set

  @XmlElementWrapper(name = "calendars")
  @XmlElement(name = "example")
  var calendars: List<Calendar> = emptyList()
    private set

  constructor(paging: Paging, calendars: List<Calendar>) {
    this.paging = paging
    this.calendars = calendars
  }
}
