package net.brandonlamb.api.example.webapi.mapper.orika

import ma.glasnost.orika.MapperFactory
import ma.glasnost.orika.impl.DefaultMapperFactory
import net.brandonlamb.api.common.configuration.converters.orika.InstantConverter
import net.brandonlamb.api.common.configuration.converters.orika.LocalDateConverter
import net.brandonlamb.api.common.configuration.converters.orika.LocalDateTimeConverter
import net.brandonlamb.api.common.configuration.converters.orika.LocalTimeConverter
import net.brandonlamb.api.example.webapi.models.Calendar
import org.apache.logging.log4j.LogManager
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Produces
import net.brandonlamb.api.example.dal.common.models.Calendar as DalModel

/**
 * This class exists to register all object mappings into the Orika MapperFactory on startup. This
 * is partly to avoid chicken before the egg on loading of mappers which rely on other mappers.
 */
open class OrikaConfig {
  private val log = LogManager.getLogger(javaClass);

  companion object {
    private val LOCAL_DATE = "localDate"
    private val LOCAL_TIME = "localTime"
    private val LOCAL_DATE_TIME = "localDateTime"
    private val INSTANT = "instant"
  }

  @ApplicationScoped
  @Produces
  open fun produceMapperFactory(): MapperFactory {
    log.info("Orika Mapper Factory Producer Initialized")

    val mapperFactory = DefaultMapperFactory.Builder().build()
    val converterFactory = mapperFactory.converterFactory

    // register common api converters
    converterFactory.registerConverter(LOCAL_DATE, LocalDateConverter())
    converterFactory.registerConverter(LOCAL_TIME, LocalTimeConverter())
    converterFactory.registerConverter(LOCAL_DATE_TIME, LocalDateTimeConverter())
    converterFactory.registerConverter(INSTANT, InstantConverter())

    mapperFactory.classMap(DalModel::class.java, Calendar::class.java).byDefault().register()

    return mapperFactory
  }
}
