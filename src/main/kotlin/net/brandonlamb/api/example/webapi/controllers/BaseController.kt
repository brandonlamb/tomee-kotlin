package net.brandonlamb.api.example.webapi.controllers

import net.brandonlamb.api.common.language.LocaleFactory
import java.util.Locale
import javax.enterprise.context.RequestScoped
import javax.ws.rs.core.Context
import javax.ws.rs.core.HttpHeaders

abstract class BaseController {
  @RequestScoped
  @Context
  private val headers: HttpHeaders? = null

  protected val locale: Locale
    get() = LocaleFactory.create(headers!!)
}
