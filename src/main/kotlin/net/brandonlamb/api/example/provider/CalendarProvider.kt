package net.brandonlamb.api.example.provider

import net.brandonlamb.api.example.dal.common.CalendarDal
import net.brandonlamb.api.example.dal.common.models.Calendar
import java.time.LocalDate
import java.util.Locale
import javax.ejb.Lock
import javax.ejb.LockType.READ
import javax.ejb.Singleton
import javax.inject.Inject

@Singleton
@Lock(READ)
open class CalendarProvider @Inject constructor(var calendarDal: CalendarDal) {
  /**
   * Find example by example date

   * @param calendarDate Calendar date
   * @param locale Locale
   * @return Calendar
   */
  open fun findByCalendarDate(calendarDate: LocalDate, locale: Locale): Calendar
    = calendarDal.findByCalendarDate(calendarDate, locale)
}
