package net.brandonlamb.api.example.dal.db2

import net.brandonlamb.api.common.dal.sql.LocaleConverter
import net.brandonlamb.api.common.dal.sql.SqlDateConverter
import net.brandonlamb.api.common.exceptions.NoResultsException
import net.brandonlamb.api.example.dal.common.CalendarDal
import net.brandonlamb.api.example.dal.common.models.Calendar
import net.brandonlamb.api.example.dal.db2.mapper.mybatis.CalendarMapper
import org.mybatis.cdi.Mapper
import java.time.LocalDate
import java.util.Locale
import javax.ejb.Lock
import javax.ejb.LockType.READ
import javax.ejb.Singleton
import javax.inject.Inject

@Singleton
@Lock(READ)
open class CalendarDalImpl @Inject constructor(
  @Mapper var calendarMapper: CalendarMapper
) : CalendarDal {
  /**
   * {@inheritDoc}
   */
  override fun findByCalendarDate(calendarDate: LocalDate, locale: Locale): Calendar {
    return calendarMapper.findByCalendarDate(
      SqlDateConverter.toDate(calendarDate)!!,
      LocaleConverter.convert(locale)
    ) ?: throw NoResultsException(212002, "calendarDate=" + calendarDate.toString())
  }
}
