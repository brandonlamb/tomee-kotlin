package net.brandonlamb.api.example.dal.common.models

import java.io.Serializable
import java.time.LocalDate

data class Calendar(
  var calendarDate: LocalDate? = null,
  var calendarDayOfWeek: Int = 0,
  var calendarDayOfYear: Int = 0,
  var calendarDayOfWeekName: String? = null,
  var calendarMonthName: String? = null,
  var fiscalMonthName: String? = null,
  var fiscalYear: Int = 0,
  var fiscalYearBeginDate: LocalDate? = null,
  var fiscalYearEndDate: LocalDate? = null,
  var fiscalYearWeeks: Int = 0,
  var fiscalPeriod: Int = 0,
  var fiscalPeriodBeginDate: LocalDate? = null,
  var fiscalPeriodEndDate: LocalDate? = null,
  var fiscalPeriodWeeks: Int = 0,
  var fiscalPeriodDays: Int = 0,
  var fiscalWeek: Int = 0,
  var fiscalWeekBeginDate: LocalDate? = null,
  var fiscalWeekEndDate: LocalDate? = null,
  var fiscalDayOfWeek: Int = 0,
  var fiscalDayOfYear: Int = 0,
  var fiscalDayPeriod: Int = 0
) : Serializable
