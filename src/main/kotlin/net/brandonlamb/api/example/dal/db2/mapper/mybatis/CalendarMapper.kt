package net.brandonlamb.api.example.dal.db2.mapper.mybatis

import net.brandonlamb.api.example.dal.common.models.Calendar
import org.apache.ibatis.annotations.Param
import java.sql.Date

interface CalendarMapper {
  fun findByCalendarDate(
    @Param("calendarDate") calendarDate: Date,
    @Param("locale") locale: String
  ): Calendar?
}
