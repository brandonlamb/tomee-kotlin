package net.brandonlamb.api.example.dal.common

import net.brandonlamb.api.example.dal.common.models.Calendar
import java.time.LocalDate
import java.util.Locale
import javax.validation.constraints.NotNull

interface CalendarDal {
  /**
   * Find example by example date

   * @param calendarDate Calendar date
   * *
   * @return Calendar
   */
  @NotNull(message = "212002")
  fun findByCalendarDate(
    calendarDate: LocalDate,
    locale: Locale): Calendar
}
