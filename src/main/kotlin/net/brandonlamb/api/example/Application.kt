package net.brandonlamb.api.example

import javax.ws.rs.ApplicationPath
import javax.ws.rs.core.Application as BaseApplication

@ApplicationPath("/")
class Application : BaseApplication()
