package net.brandonlamb.api.common.converter

import net.brandonlamb.api.common.exceptions.ValidationException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.DateTimeException
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.format.DateTimeParseException
import java.util.Date

object DateConverter {
  /**
   * @param date the date string to be parsed into a date object
   * @return A Date object containing the parsed date, or null if date is not specified and not
   * required.
   * @throws ValidationException if date cannot be parsed, or date is required and no value is
   *                             specified.
   */
  fun toDate(date: String): Date? = toDate(date, 100007)

  /**
   * @param date      the date string to be parsed into a date object
   * @param errorCode the error code number to look up api error message
   * @return A Date object containing the parsed date, or null if date is not specified and not
   * required.
   * @throws ValidationException if date cannot be parsed, or date is required and no value is
   *                             specified.
   */
  fun toDate(date: String?, errorCode: Int): Date? {
    if (date == null) {
      return null
    }

    try {
      return SimpleDateFormat("yyyy-MM-dd").parse(date)
    } catch (e: NullPointerException) {
      throw ValidationException(errorCode, "A required date field is null.")
    } catch (e: ParseException) {
      throw ValidationException(errorCode, "Unable to parse date: $date", e)
    }
  }

  /**
   * Converts a String to a LocalTime object
   *
   * @param date String value to be converted to null
   * @return null if not required
   */
  fun toLocalTime(date: String): LocalTime? = toLocalTime(date, 100007)

  /**
   * Converts a String to a LocalTime object
   *
   * @param date      String value to be converted to null
   * @param errorCode the error code number to look up api error message
   * @return null if not required
   */
  fun toLocalTime(date: String?, errorCode: Int): LocalTime? {
    if (date == null) {
      return null
    }

    try {
      return LocalTime.parse(date)
    } catch (e: NullPointerException) {
      throw ValidationException(errorCode, "A required date field is null")
    } catch (e: DateTimeParseException) {
      throw ValidationException(errorCode, "Unable to parse date: $date")
    }
  }

  /**
   * Converts a String to a Instant object
   *
   * @param date String value to be converted to null
   * @return null if not required
   */
  fun toInstant(date: String?): Instant? = toInstant(date, 100007)

  /**
   * Converts a String to a Instant object
   *
   * @param date      String value to be converted to null
   * @param errorCode the error code number to look up api error message
   * @return null if not required
   */
  fun toInstant(date: String?, errorCode: Int): Instant? {
    if (date == null) {
      return null
    }

    try {
      return Instant.parse(date)
    } catch (e: NullPointerException) {
      throw ValidationException(errorCode, "A required date field is null")
    } catch (e: DateTimeParseException) {
      throw ValidationException(errorCode, "Unable to parse date: " + date)
    }
  }

  /**
   * Converts a String to a LocalDate object
   *
   * @param date String value to be converted to null
   * @return null if not required
   */
//  fun toLocalDate(date: String?): LocalDate? = toLocalDate(date, 100007)

  /**
   * Converts a String to a LocalDate object
   *
   * @param date      String value to be converted to null
   * @param errorCode the error code number to look up api error message
   * @return null if not required
   */
  fun toLocalDate(date: String?, errorCode: Int = 100007): LocalDate? {
    if (date == null) {
      return null
    }

    try {
      return LocalDate.parse(date)
    } catch (e: NullPointerException) {
      throw ValidationException(errorCode, "A required date field is null")
    } catch (e: DateTimeParseException) {
      throw ValidationException(errorCode, "Unable to parse date: $date")
    }
  }

  /**
   * Converts input values of year,month and date into a LocalDate object that is validated for
   * usability with db2 date type
   *
   * @param year  int value for year number, valid value between 1000-9999
   * @param month int value for month number, valid value between 1-12
   * @param day   int value for day number, depending on month value
   * @return a LocalDate instance with the given inputs
   * @throws ValidationException if year is not in range or invalid date/month value
   */
  fun toLocalDate(year: Int, month: Int, day: Int): LocalDate? = toLocalDate(year, month, day, 100007)

  /**
   * Converts input values of year,month and date into a LocalDate object that is validated for
   * usability with db2 date type
   *
   * @param year      int value for year number, valid value between 1000-9999
   * @param month     int value for month number, valid value between 1-12
   * @param day       int value for day number, depending on month value
   * @param errorCode the error code number to look up api error message
   * @return a LocalDate instance with the given inputs
   * @throws ValidationException if year is not in range or invalid date/month value
   */
  fun toLocalDate(year: Int, month: Int, day: Int, errorCode: Int): LocalDate? {
    // db2 date format definition:
    // A date is a three-part value representing a year, month, and day
    // in the range of 0001-01-01 to 9999-12-31.
    if (year < 1 || year > 9999) {
      // LocalDate can take any number for year, however Starbucks db2 is capped at 9999
      throw ValidationException(
        errorCode,
        "Invalid year value (must be between 1 and 9999): " + year)
    }

    try {
      return LocalDate.of(year, month, day)
    } catch (e: DateTimeException) {
      throw ValidationException(errorCode, e.message.toString())
    }
  }

  /**
   * Converts a String to a OffsetDateTime object
   *
   * @param date timestamp in ISO 8601 Format
   * @return offsetDateTime object from String
   */
  fun toOffSetDateTime(date: String?): OffsetDateTime? = toOffSetDateTime(date, 100007)

  /**
   * Converts a String to a OffsetDateTime object
   *
   * @param date      timestamp in ISO 8601 Format
   * @param errorCode the error code number to look up api error message
   * @return offsetDateTime object from String
   */
  fun toOffSetDateTime(date: String?, errorCode: Int): OffsetDateTime? {
    try {
      return OffsetDateTime.parse(date)
    } catch (e: NullPointerException) {
      throw ValidationException(errorCode, "A required date field is null")
    } catch (e: DateTimeParseException) {
      throw ValidationException(errorCode, "Unable to parse date: $date")
    }
  }

  /**
   * Converts a possibly null Instant value to a String
   *
   * @param instant value to be converted
   * @return value in ISO-8601 format or null
   */
  fun asString(instant: Instant?): String? = instant?.toString()

  /**
   * Converts a possibly null Date value into a String
   *
   * @param date value to be converted
   * @return value in yyyy-MM-dd format or null
   */
  fun asString(date: Date?): String? = if (date == null)
    null else SimpleDateFormat("yyyy-MM-dd").format(date)

  /**
   * Converts a possibly null LocalTime value to a String
   *
   * @param localTime value to be converted
   * @return value in HH:MM:SS format or null
   */
  fun asString(localTime: LocalTime?): String? = localTime?.toString()

  /**
   * Converts a possible null LocalDate object to a String value
   *
   * @param localDate LocalDate object
   * @return null or a string value in yyyy-MM-DD format
   */
  fun asString(localDate: LocalDate?): String? = localDate?.toString()

  fun asString(localDateTime: LocalDateTime?): String? = localDateTime?.toString()

  /**
   * Converts a possible null LocalDate object to a String value
   *
   * @param offsetDateTime OffsetDateTime object
   * @return null or a string value in yyyy-MM-DD format
   */
  fun asString(offsetDateTime: OffsetDateTime?): String? = offsetDateTime?.toString()
}
