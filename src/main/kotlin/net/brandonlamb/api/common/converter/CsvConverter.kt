package net.brandonlamb.api.common.converter

object CsvConverter {
  /**
   * @param values should be a non null String value in Csv format I.e. val1,val2,val3,...,valn
   * @return a list containin the parsed values.
   * @throws IllegalArgumentException if a null value is passed in or unable to be parsed
   */
  @Throws(IllegalArgumentException::class, NumberFormatException::class)
  fun toIntList(values: String?): List<Int> = if (values == null || values.length == 0)
    emptyList() else values.split(",").map { it.toInt() }

  /**
   * @param values should be a non null String value in Csv format I.e. val1,val2,val3,...,valn
   * @return a list containin the parsed values.
   * @throws IllegalArgumentException if a null value is passed in or unable to be parsed
   */
  @Throws(IllegalArgumentException::class, NumberFormatException::class)
  fun toStringList(values: String?): List<String> = if (values == null || values.length == 0)
    emptyList() else values.split(",").map { it.trim() }

  /**
   * This method converts a List into a Csv String value.
   * @param values should be a non null List to be converted to a CSV.
   * @return a Csv string value
   */
  fun toCsvString(values: List<Int>): String
    = values.map { it.toString().trim() }.joinToString(",")
}
