package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import javax.inject.Inject
import javax.ws.rs.ClientErrorException
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.BAD_REQUEST
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class ClientErrorExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<ClientErrorException> {
  companion object {
    private val log = LogManager.getLogger(ClientErrorExceptionMapper::class.java)
    private val code = 100013
  }

  override fun toResponse(e: ClientErrorException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(BAD_REQUEST).entity(
      ApiError(BAD_REQUEST.statusCode, codeMapper.map(code))
    ).build()
  }
}
