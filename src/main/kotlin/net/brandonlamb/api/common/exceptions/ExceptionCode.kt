package net.brandonlamb.api.common.exceptions

interface ExceptionCode {
  /**
   * Get the code used to identify specific error

   * @return int
   */
  val code: Int
}
