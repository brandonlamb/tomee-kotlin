package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.exceptions.RequiredParameterException
import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.BAD_REQUEST
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class RequiredParameterExceptionMapper : ExceptionMapper<RequiredParameterException> {
  companion object {
    private val log = LogManager.getLogger(RequiredParameterExceptionMapper::class.java)
    private val code = 100101
  }

  override fun toResponse(e: RequiredParameterException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(BAD_REQUEST).entity(
      ApiError(code, "A required parameter is missing: \"" + e.message + "\"")
    ).build()
  }
}
