package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.exceptions.BadRequestException
import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import javax.inject.Inject
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.BAD_REQUEST
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class BadRequestExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<BadRequestException> {
  companion object {
    private val log = LogManager.getLogger(BadRequestException::class.java)
  }

  override fun toResponse(e: BadRequestException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(BAD_REQUEST).entity(
      ApiError(e.code, codeMapper.map(e.code))
    ).build()
  }
}
