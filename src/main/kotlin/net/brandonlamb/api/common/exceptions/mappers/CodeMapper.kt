package net.brandonlamb.api.common.exceptions.mappers

/**
 * Map a code to an error message. Allow for multiple implementations in case we need to break out
 * layers
 */
interface CodeMapper {
  /**
   * Map a code to a string message

   * @param code int code
   * *
   * @return String message
   */
  fun map(code: Int): String
}
