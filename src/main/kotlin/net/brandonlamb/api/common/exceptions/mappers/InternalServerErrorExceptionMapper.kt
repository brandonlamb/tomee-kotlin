package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.exceptions.InternalServerErrorException
import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import javax.inject.Inject
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class InternalServerErrorExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<InternalServerErrorException> {
  companion object {
    private val log = LogManager.getLogger(InternalServerErrorExceptionMapper::class.java)
  }

  override fun toResponse(e: InternalServerErrorException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(INTERNAL_SERVER_ERROR).entity(
      ApiError(e.code, codeMapper.map(e.code))
    ).build()
  }
}
