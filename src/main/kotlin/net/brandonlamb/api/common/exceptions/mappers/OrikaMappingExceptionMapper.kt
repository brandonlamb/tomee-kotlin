package net.brandonlamb.api.common.exceptions.mappers

import ma.glasnost.orika.MappingException
import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import javax.inject.Inject
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class OrikaMappingExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<MappingException> {
  companion object {
    private val log = LogManager.getLogger(OrikaMappingExceptionMapper::class.java)
    private val code = 100014
  }

  override fun toResponse(e: MappingException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(INTERNAL_SERVER_ERROR).entity(
      ApiError(INTERNAL_SERVER_ERROR.statusCode, codeMapper.map(code))
    ).build()
  }
}
