package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import org.xml.sax.SAXParseException
import javax.inject.Inject
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class SAXParserExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<SAXParseException> {
  companion object {
    private val log = LogManager.getLogger(SAXParserExceptionMapper::class.java)
    private val code = 100001
  }

  override fun toResponse(e: SAXParseException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(INTERNAL_SERVER_ERROR).entity(
      ApiError(INTERNAL_SERVER_ERROR.statusCode, codeMapper.map(code))
    ).build()
  }
}
