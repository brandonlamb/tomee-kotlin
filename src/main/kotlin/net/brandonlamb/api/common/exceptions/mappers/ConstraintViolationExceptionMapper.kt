package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import javax.inject.Inject
import javax.validation.ConstraintViolationException
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.BAD_REQUEST
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class ConstraintViolationExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<ConstraintViolationException> {
  companion object {
    private val log = LogManager.getLogger(ConstraintViolationExceptionMapper::class.java)
    private val DEFAULT_CODE = 100008
    private val EMBEDDED_CODE = 100009
  }

  override fun toResponse(e: ConstraintViolationException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    val message = e.constraintViolations.firstOrNull()?.message
    var code: Int? = null

    try {
      code = Integer.valueOf(message)
      return Response.status(BAD_REQUEST).entity(ApiError(code, codeMapper.map(code))).build()
    } catch (nfe: NumberFormatException) {
      //developer may put message(or null) instead of the code
      return Response.status(BAD_REQUEST).entity(
        ApiError(
          if (code == null) DEFAULT_CODE else EMBEDDED_CODE,
          message ?: codeMapper.map(DEFAULT_CODE)
        )).build()
    }
  }
}
