package net.brandonlamb.api.common.exceptions.mappers

import org.apache.logging.log4j.LogManager
import javax.annotation.Priority
import javax.interceptor.AroundInvoke
import javax.interceptor.Interceptor
import javax.interceptor.InvocationContext

@Loggable
@Interceptor
@Priority(Interceptor.Priority.APPLICATION)
class LoggableInterceptor {
  companion object {
    private val log = LogManager.getLogger()
  }

  @AroundInvoke
  @Throws(Exception::class)
  fun log(ctx: InvocationContext): Any {
//    val e: Exception = (Exception) ctx.target

//    val sb = StringBuilder();
//    ctx.parameters.map { sb.append(it.toString()) }
//    for (Object obj : ic.getParameters()) {
//      sb.append(obj.toString());
//      sb.append(", ");
//    }
//    sb.append("]");

//    log.info(e.getM)
//    log.debug("debug")

    return ctx.proceed()
  }
}
