package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import java.net.SocketTimeoutException
import javax.inject.Inject
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.BAD_REQUEST
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class SocketTimeoutExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<SocketTimeoutException> {
  companion object {
    private val log = LogManager.getLogger(SocketTimeoutExceptionMapper::class.java)
    private val code = 100015
  }

  override fun toResponse(e: SocketTimeoutException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(BAD_REQUEST).entity(
      ApiError(BAD_REQUEST.statusCode, codeMapper.map(code))
    ).build()
  }
}
