package net.brandonlamb.api.common.exceptions

import javax.ejb.ApplicationException

@ApplicationException(inherited = true, rollback = false)
abstract class BaseExceptionImpl : RuntimeException, ExceptionCode {
  override val code: Int = 0

  constructor(message: String) : super(message)

  constructor(code: Int) : super()

  constructor(e: Exception) : super(e)

  constructor(code: Int, message: String) : super(message)

  constructor(message: String, e: Exception) : super(message, e)

  constructor(code: Int, message: String, e: Exception) : super(message, e)
}
