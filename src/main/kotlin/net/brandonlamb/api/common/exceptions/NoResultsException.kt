package net.brandonlamb.api.common.exceptions

class NoResultsException : BaseExceptionImpl, ProviderException {
  constructor(message: String) : super(message)

  constructor(code: Int) : super(code)

  constructor(e: Exception) : super(e)

  constructor(code: Int, message: String) : super(code, message)

  constructor(message: String, e: Exception) : super(message, e)

  constructor(code: Int, message: String, e: Exception) : super(code, message, e)
}
