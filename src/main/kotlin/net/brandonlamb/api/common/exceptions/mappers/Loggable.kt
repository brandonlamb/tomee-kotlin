package net.brandonlamb.api.common.exceptions.mappers

import java.lang.annotation.Inherited
import javax.interceptor.InterceptorBinding

@Inherited
@InterceptorBinding
@Retention(AnnotationRetention.RUNTIME)
@Target(
  AnnotationTarget.FUNCTION,
  AnnotationTarget.TYPE,
  AnnotationTarget.CLASS
)
annotation class Loggable
