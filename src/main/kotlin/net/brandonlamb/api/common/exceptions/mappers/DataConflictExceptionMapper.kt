package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.exceptions.DataConflictException
import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import javax.inject.Inject
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.CONFLICT
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class DataConflictExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<DataConflictException> {
  companion object {
    private val log = LogManager.getLogger(DataConflictExceptionMapper::class.java)
    private val code = 100013
  }

  override fun toResponse(e: DataConflictException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(CONFLICT).entity(
      ApiError(code, codeMapper.map(code))
    ).build()
  }
}
