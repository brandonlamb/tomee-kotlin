package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.webapi.models.ApiError
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class NullPointerExceptionMapper : ExceptionMapper<NullPointerException> {
  companion object {
    private val CODE = 100005
    private val MESSAGE = "A null object was referenced"
  }

  override fun toResponse(e: NullPointerException): Response
    = Response.status(INTERNAL_SERVER_ERROR).entity(ApiError(CODE, MESSAGE)).build()
}
