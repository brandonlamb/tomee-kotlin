package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.johnzon.mapper.MapperException
import org.apache.logging.log4j.LogManager
import javax.inject.Inject
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class JohnzonMapperExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<MapperException> {
  companion object {
    private val log = LogManager.getLogger(JohnzonMapperExceptionMapper::class.java)
    private val code = 100012
  }

  override fun toResponse(e: MapperException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(INTERNAL_SERVER_ERROR).entity(
      ApiError(code, codeMapper.map(code))
    ).build()
  }
}
