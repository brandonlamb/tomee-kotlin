package net.brandonlamb.api.common.exceptions

/**
 * Marker interface for provider exceptions
 */
interface ProviderException
