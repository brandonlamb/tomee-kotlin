package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.webapi.models.ApiError
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.BAD_REQUEST
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class IllegalArgumentExceptionMapper<T : IllegalArgumentException> : ExceptionMapper<T> {
  override fun toResponse(e: T): Response
    = Response.status(BAD_REQUEST).entity(ApiError(BAD_REQUEST.statusCode, e.message)).build()
}
