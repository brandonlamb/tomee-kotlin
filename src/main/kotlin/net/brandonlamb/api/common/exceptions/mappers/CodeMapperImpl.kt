package net.brandonlamb.api.common.exceptions.mappers

import javax.ejb.Lock
import javax.ejb.LockType.READ
import javax.ejb.Singleton
import javax.inject.Inject

@Singleton
@Lock(READ)
class CodeMapperImpl @Inject constructor(@ApiErrorCode val codes: Map<Int, String>) : CodeMapper {
  override fun map(code: Int): String = codes.getOrElse(code, { "Undefined String" })
}
