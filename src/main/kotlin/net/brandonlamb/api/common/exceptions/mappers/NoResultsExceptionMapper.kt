package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.exceptions.NoResultsException
import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import javax.inject.Inject
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.NOT_FOUND
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class NoResultsExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<NoResultsException> {
  companion object {
    private val log = LogManager.getLogger(NoResultsExceptionMapper::class.java)
  }

  override fun toResponse(e: NoResultsException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(NOT_FOUND).entity(
      ApiError(e.code, codeMapper.map(e.code))
    ).build()
  }
}
