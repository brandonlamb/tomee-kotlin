package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import javax.inject.Inject
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class NumberFormatExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<NumberFormatException> {
  companion object {
    private val log = LogManager.getLogger(NumberFormatExceptionMapper::class.java)
    private val code = 100011
  }

  override fun toResponse(e: NumberFormatException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(INTERNAL_SERVER_ERROR).entity(
      ApiError(INTERNAL_SERVER_ERROR.statusCode, codeMapper.map(code))
    ).build()
  }
}
