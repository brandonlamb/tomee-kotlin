package net.brandonlamb.api.common.exceptions.mappers

import net.brandonlamb.api.common.exceptions.DataSourceException
import net.brandonlamb.api.common.webapi.models.ApiError
import org.apache.logging.log4j.LogManager
import javax.inject.Inject
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class DataSourceExceptionMapper @Inject constructor(
  val codeMapper: CodeMapper
) : ExceptionMapper<DataSourceException> {
  companion object {
    private val log = LogManager.getLogger(DataSourceExceptionMapper::class.java)
  }

  override fun toResponse(e: DataSourceException): Response {
    log.info(e.message ?: "No exception message")
    log.debug(e)

    return Response.status(INTERNAL_SERVER_ERROR).entity(
      ApiError(e.code, codeMapper.map(e.code))
    ).build()
  }
}
