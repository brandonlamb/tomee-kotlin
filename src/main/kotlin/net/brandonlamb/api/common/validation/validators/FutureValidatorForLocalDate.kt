package net.brandonlamb.api.common.validation.validators

import net.brandonlamb.api.common.validation.constraints.Future

import java.time.LocalDate

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class FutureValidatorForLocalDate : ConstraintValidator<Future, LocalDate> {
  override fun initialize(constraintAnnotation: Future) = Unit

  override fun isValid(date: LocalDate?, context: ConstraintValidatorContext): Boolean
    = date == null || date.isAfter(LocalDate.now())
}
