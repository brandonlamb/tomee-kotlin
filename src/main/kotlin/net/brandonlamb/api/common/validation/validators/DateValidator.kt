package net.brandonlamb.api.common.validation.validators

import java.text.ParseException
import java.text.SimpleDateFormat

object DateValidator {
  fun validate(date: String): Boolean {
    try {
      val formatter = SimpleDateFormat("yyyy-MM-dd")
      formatter.isLenient = false

      val parsedDate = formatter.parse(date)
      return true
    } catch (e: ParseException) {
      e.printStackTrace()
      return false
    }
  }
}
