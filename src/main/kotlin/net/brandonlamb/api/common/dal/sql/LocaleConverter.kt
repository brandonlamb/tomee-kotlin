package net.brandonlamb.api.common.dal.sql

import java.util.Locale

object LocaleConverter {
  private val EN = "en"
  private val DE = "de"
  private val FR = "fr"
  private val CZ = "cz"

  fun convert(locale: Locale?): String {
    if (locale == null) {
      return EN
    }

    if (Locale.GERMAN == locale || Locale.GERMANY == locale) {
      return DE
    } else if (Locale.FRENCH == locale || Locale.FRANCE == locale) {
      return FR
    } else if (Locale.CHINA == locale || Locale.CHINESE == locale) {
      return CZ
    }

    return EN
  }
}
