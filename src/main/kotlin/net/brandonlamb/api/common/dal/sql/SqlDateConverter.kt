package net.brandonlamb.api.common.dal.sql

import java.sql.Time
import java.sql.Timestamp
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
<<<<<<< HEAD
import java.util.*
=======
import java.util.Date
>>>>>>> 9cadce35f8703717cf106e699a8696f703da86e2

object SqlDateConverter {
  /**
   * Convert a nullable util.Date into a sql.Date

   * @param date java.til.Date
   * *
   * @return java.sql.Date
   */
  fun toDate(date: Date?): java.sql.Date? = if (date == null) null else java.sql.Date(date.time)

  /**
   * Convert a nullable LocalDate into a sql.Date

   * @param localDate LocalDate
   * *
   * @return java.sql.Date
   */
  fun toDate(localDate: LocalDate?): java.sql.Date? = if (localDate == null)
    null else toDate(Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()))

  /**
   * Convert a nullable sql.Date into a util.Date

   * @param date java.sql.Date
   * *
   * @return java.util.Date
   */
  fun toDate(date: java.sql.Date?): Date? = if (date == null) null else Date(date.time)

  /**
   * Convert a nullable sql.Date into a LocalDate

   * @param date java.sql.Date
   * *
   * @return LocalDate
   */
  fun toLocalDate(date: java.sql.Date?): LocalDate? = date?.toLocalDate()

  /**
   * Convert a nullable sql.Time into a LocalTime

   * @param time java.sql.Time
   * *
   * @return LocalTime
   */
  fun toLocalTime(time: Time?): LocalTime? = time?.toLocalTime()

  /**
   * Convert LocalTime to java.sql.Time

   * @param localTime LocalTime
   * *
   * @return java.sql.Time
   */
  fun toTime(localTime: LocalTime?): Time? = if (localTime == null) null else Time.valueOf(localTime)

  /**
   * Convert a nullable sql.Timestamp into a LocalDateTime

   * @param timestamp java.sql.Timestamp
   * *
   * @return LocalDateTime
   */
  fun toLocalDateTime(timestamp: Timestamp?): LocalDateTime? = timestamp?.toLocalDateTime()

  /**
   * Convert a LocalDateTime to a java.sql.Timestamp

   * @param localDateTime LocalDateTime
   * *
   * @return java.sql.Timestamp
   */
  fun toTimestamp(localDateTime: LocalDateTime?): Timestamp? = if (localDateTime == null)
    null else Timestamp.valueOf(localDateTime)

  /**
   * Convert a nullable sql.Timestamp into an Instant

   * @param timestamp java.sql.Timestamp
   * *
   * @return Instant
   */
  fun toInstant(timestamp: Timestamp?): Instant? = timestamp?.toInstant()
}
