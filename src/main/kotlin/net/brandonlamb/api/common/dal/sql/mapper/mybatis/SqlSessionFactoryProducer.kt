package net.brandonlamb.api.common.dal.sql.mapper.mybatis

import net.brandonlamb.api.common.configuration.mappers.MyBatisConfig
import org.apache.ibatis.io.Resources
import org.apache.ibatis.session.SqlSessionFactory
import org.apache.ibatis.session.SqlSessionFactoryBuilder
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Produces
import javax.inject.Inject

class SqlSessionFactoryProducer @Inject constructor(val myBatisConfig: MyBatisConfig) {
  @ApplicationScoped
  @Produces
  fun createFactory(): SqlSessionFactory = SqlSessionFactoryBuilder().build(
    Resources.getResourceAsStream(myBatisConfig.fileName())
  )
}
