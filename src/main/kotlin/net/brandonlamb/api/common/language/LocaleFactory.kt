package net.brandonlamb.api.common.language

import java.util.Locale
import javax.ws.rs.core.HttpHeaders

object LocaleFactory {
  fun create(headers: HttpHeaders): Locale {
    if (headers.acceptableLanguages.size > 0) {
      return Locale(headers.acceptableLanguages[0].language.toString())
    }

    return Locale.ENGLISH
  }
}
