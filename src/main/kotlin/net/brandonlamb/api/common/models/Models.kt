package net.brandonlamb.api.common.models

data class Models<T>(var models: MutableList<T>, var total: Int)
