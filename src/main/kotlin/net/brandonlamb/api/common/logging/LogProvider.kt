package net.brandonlamb.api.common.logging

import org.apache.logging.log4j.LogManager
import javax.enterprise.inject.Produces
import javax.enterprise.inject.spi.InjectionPoint

class LogProvider {
  @Produces
  fun createLogger(ip: InjectionPoint) = LogManager.getLogger(ip.member.declaringClass.name)
}
