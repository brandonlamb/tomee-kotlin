package net.brandonlamb.api.common.configuration.converters.orika

import ma.glasnost.orika.MappingContext
import ma.glasnost.orika.converter.BidirectionalConverter
import ma.glasnost.orika.metadata.Type
import net.brandonlamb.api.common.converter.DateConverter
import java.time.Instant

class InstantConverter : BidirectionalConverter<Instant, String>() {
  override fun convertTo(
    source: Instant?,
    destinationType: Type<String>,
    mappingContext: MappingContext
  ): String? = DateConverter.asString(source)

  override fun convertFrom(
    source: String?,
    destinationType: Type<Instant>,
    mappingContext: MappingContext
  ): Instant? = DateConverter.toInstant(source)
}
