package net.brandonlamb.api.common.configuration.models

data class Header(var headerName: String? = null, var headerValue: String? = null)
