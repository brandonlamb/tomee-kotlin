package net.brandonlamb.api.common.configuration.producers

import net.brandonlamb.api.common.configuration.mappers.ErrorCodeConfig
import net.brandonlamb.api.common.exceptions.InternalServerErrorException
import net.brandonlamb.api.common.exceptions.mappers.ApiErrorCode
import org.apache.ibatis.io.Resources
import java.io.IOException
import java.util.HashMap
import java.util.Properties
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Produces
import javax.inject.Inject

class ErrorCodeProducer @Inject constructor(val errorCodeConfig: ErrorCodeConfig) {
  @ApplicationScoped
  @Produces
  @ApiErrorCode
  fun getErrorsMaps(): Map<Int, String> {
    try {
      val properties = Properties()
      val errors = HashMap<Int, String>()
      properties.load(Resources.getResourceAsStream(errorCodeConfig.getFileName()))
      properties.stringPropertyNames().forEach { i -> errors.put(i.toInt(), properties[i] as String) }

      return errors
    } catch (e: IOException) {
      throw InternalServerErrorException(e)
    } catch (e: NumberFormatException) {
      throw InternalServerErrorException(e)
    }
  }
}
