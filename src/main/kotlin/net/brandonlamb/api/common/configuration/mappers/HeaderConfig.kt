package net.brandonlamb.api.common.configuration.mappers

import net.brandonlamb.api.common.configuration.converters.HeaderConverter
import net.brandonlamb.api.common.configuration.models.Header
import org.aeonbits.owner.Config
import java.util.concurrent.TimeUnit

@Config.Sources(
  "file:\${CATALINA_BASE}/conf/override.properties",
  "classpath:conf/\${API_ENV}.properties"
)
@Config.HotReload(value = 2, unit = TimeUnit.MINUTES, type = Config.HotReloadType.ASYNC)
@Config.LoadPolicy(Config.LoadType.MERGE)
interface HeaderConfig : Config {
  @Config.Key("http.response.headers")
  @Config.DefaultValue(
    "Access-Control-Allow-Headers: Accept, Authorization, Origin, Content-Type ||" +
      "Access-Control-Allow-Origin: * ||" +
      "Access-Control-Allow-Methods: GET, POST, DELETE, PUT, OPTIONS, HEAD ||" +
      "X-Responded-By: Gred-Custom-Filter")
  @Config.ConverterClass(HeaderConverter::class)
  @Config.Separator("\\|\\|")
  fun getResponseHeaders(): List<Header>

  @Config.Key("response.headers.enabled")
  @Config.DefaultValue("true")
  fun getEnabled(): Boolean
}
