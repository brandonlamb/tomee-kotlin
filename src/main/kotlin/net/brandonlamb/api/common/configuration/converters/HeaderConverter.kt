package net.brandonlamb.api.common.configuration.converters

import net.brandonlamb.api.common.configuration.models.Header
import org.aeonbits.owner.Converter
import java.lang.reflect.Method
import javax.ejb.Lock
import javax.ejb.LockType.READ
import javax.ejb.Singleton

@Singleton
@Lock(READ)
class HeaderConverter : Converter<Header> {
  /**
   * @param targetMethod used by the framework. Should be the value of the reflected method
   * *
   * @param text         expected text is a header in the following formats: HeaderName: Value OR
   * *                     HeaderName
   * *
   * @return a header object to be added to the filter
   */
  override fun convert(targetMethod: Method, text: String): Header {
    try {
      val split = text.split(":".toRegex(), 2).toTypedArray()
      val header = Header(split[0], if (split.size == 2) split[1] else "")
      return header
    } catch (e: Exception) {
      throw IllegalArgumentException(
        "Header value should be in HeaderName: Value OR HeaderName format $text", e
      )
    }
  }
}
