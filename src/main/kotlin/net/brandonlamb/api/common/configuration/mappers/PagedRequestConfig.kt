package net.brandonlamb.api.common.configuration.mappers

import org.aeonbits.owner.Config
import java.util.concurrent.TimeUnit

@Config.Sources(
  "file:\${CATALINA_BASE}/conf/override.properties",
  "classpath:conf/\${API_ENV}.properties"
)
@Config.HotReload(value = 2, unit = TimeUnit.MINUTES, type = Config.HotReloadType.ASYNC)
@Config.LoadPolicy(Config.LoadType.MERGE)
interface PagedRequestConfig : Config {
  var limit: Int
    @Config.Key("api.limit") @Config.DefaultValue("1") set

  var limitMax: Int
    @Config.Key("api.limit.max") @Config.DefaultValue("1") set

  var offset: Int
    @Config.Key("api.offset") @Config.DefaultValue("0") set
}
