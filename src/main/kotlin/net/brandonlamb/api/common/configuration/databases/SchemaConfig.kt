package net.brandonlamb.api.common.configuration.databases

import org.aeonbits.owner.Config

@Config.Sources(
  "file:\${CATALINA_BASE}/conf/override.properties",
  "classpath:conf/\${API_ENV}.properties"
)
interface SchemaConfig : Config {
  @Config.Key("db.defaultSchema")
  fun getDefaultSchema(): String
}
