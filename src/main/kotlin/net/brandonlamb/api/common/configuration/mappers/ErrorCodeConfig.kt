package net.brandonlamb.api.common.configuration.mappers

import org.aeonbits.owner.Config

@Config.Sources(
  "file:\${CATALINA_BASE}/conf/override.properties",
  "classpath:conf/\${API_ENV}.properties"
)
interface ErrorCodeConfig : Config {
  @Config.Key("api.codes.filename")
  @Config.DefaultValue("api.codes.properties")
  fun getFileName(): String
}
