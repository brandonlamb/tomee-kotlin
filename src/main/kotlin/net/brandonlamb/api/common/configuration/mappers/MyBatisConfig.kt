package net.brandonlamb.api.common.configuration.mappers

import org.aeonbits.owner.Config
import java.util.concurrent.TimeUnit

@Config.Sources(
  "file:\${CATALINA_BASE}/conf/override.properties",
  "classpath:conf/\${API_ENV}.properties"
)
@Config.HotReload(value = 2, unit = TimeUnit.MINUTES, type = Config.HotReloadType.ASYNC)
@Config.LoadPolicy(Config.LoadType.MERGE)
interface MyBatisConfig : Config {
  @Config.Key("mybatis.config.xml")
  @Config.DefaultValue("mybatis.xml")
  fun fileName(): String
}
