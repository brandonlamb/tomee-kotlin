package net.brandonlamb.api.common.configuration.producers

import net.brandonlamb.api.common.configuration.databases.SchemaConfig
import net.brandonlamb.api.common.configuration.mappers.ErrorCodeConfig
import net.brandonlamb.api.common.configuration.mappers.HeaderConfig
import net.brandonlamb.api.common.configuration.mappers.MyBatisConfig
import net.brandonlamb.api.common.configuration.mappers.PagedRequestConfig

import org.aeonbits.owner.ConfigCache

import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Produces

class ConfigurationProducer {
  @ApplicationScoped
  @Produces
  fun getPagedRequestConfig(): PagedRequestConfig = ConfigCache.getOrCreate(PagedRequestConfig::class.java)

  @ApplicationScoped
  @Produces
  fun getHeaderConfig(): HeaderConfig = ConfigCache.getOrCreate(HeaderConfig::class.java)

  @ApplicationScoped
  @Produces
  fun getMyBatisConfig(): MyBatisConfig = ConfigCache.getOrCreate(MyBatisConfig::class.java)

  @ApplicationScoped
  @Produces
  fun getErrorCodeConfig(): ErrorCodeConfig = ConfigCache.getOrCreate(ErrorCodeConfig::class.java)

  @ApplicationScoped
  @Produces
  fun getSchemaConfig(): SchemaConfig = ConfigCache.getOrCreate(SchemaConfig::class.java)
}
