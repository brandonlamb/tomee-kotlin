package net.brandonlamb.api.common.configuration.converters.orika

import ma.glasnost.orika.MappingContext
import ma.glasnost.orika.converter.BidirectionalConverter
import ma.glasnost.orika.metadata.Type
import net.brandonlamb.api.common.converter.DateConverter
import java.time.OffsetDateTime

class OffsetDateTimeConverter : BidirectionalConverter<OffsetDateTime, String>() {
  override fun convertTo(
    source: OffsetDateTime?,
    destinationType: Type<String>,
    mappingContext: MappingContext
  ): String? = DateConverter.asString(source)

  override fun convertFrom(
    source: String?,
    destinationType: Type<OffsetDateTime>,
    mappingContext: MappingContext
  ): OffsetDateTime? = OffsetDateTime.parse(source)
}
