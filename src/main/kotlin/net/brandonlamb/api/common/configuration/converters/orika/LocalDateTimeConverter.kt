package net.brandonlamb.api.common.configuration.converters.orika

import ma.glasnost.orika.MappingContext
import ma.glasnost.orika.converter.BidirectionalConverter
import ma.glasnost.orika.metadata.Type
import net.brandonlamb.api.common.converter.DateConverter
import java.time.LocalDateTime

class LocalDateTimeConverter : BidirectionalConverter<LocalDateTime, String>() {
  override fun convertTo(
    source: LocalDateTime?,
    destinationType: Type<String>,
    mappingContext: MappingContext
  ): String? = DateConverter.asString(source)

  override fun convertFrom(
    source: String?,
    destinationType: Type<LocalDateTime>,
    mappingContext: MappingContext
  ): LocalDateTime? = LocalDateTime.parse(source)
}
