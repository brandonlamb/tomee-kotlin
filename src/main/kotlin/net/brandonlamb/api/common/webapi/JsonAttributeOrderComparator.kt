package net.brandonlamb.api.common.webapi

import java.util.Comparator

class JsonAttributeOrderComparator : Comparator<String> {
  override fun compare(o1: String, o2: String): Int {
    return o1.compareTo(o2)
  }

  override fun reversed(): Comparator<String> {
    return Comparator.reverseOrder<String>()
  }
}
