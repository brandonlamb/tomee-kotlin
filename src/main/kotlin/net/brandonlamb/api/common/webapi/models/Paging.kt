package net.brandonlamb.api.common.webapi.models

import javax.xml.bind.annotation.XmlAccessOrder
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorOrder
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "paging")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlAccessorOrder(XmlAccessOrder.ALPHABETICAL)
data class Paging(
  var total: Int = 0,
  var limit: Int = 0,
  var offset: Int = 0,
  var returned: Int = 0
)
