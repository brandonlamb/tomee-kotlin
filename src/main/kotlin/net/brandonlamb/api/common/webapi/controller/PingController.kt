package net.brandonlamb.api.common.webapi.controller

import net.brandonlamb.api.common.webapi.models.Ping
import java.time.LocalDate
import javax.ejb.Lock
import javax.ejb.LockType.READ
import javax.ejb.Singleton
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.MediaType.APPLICATION_XML
import javax.ws.rs.core.Response

@Singleton
@Lock(READ)
@Consumes(APPLICATION_JSON, APPLICATION_XML)
@Produces(APPLICATION_JSON, APPLICATION_XML)
class PingController {
  @GET
  @Path("/ping")
  fun pong() = Response.ok(Ping(message = "Pong", timeStamp = LocalDate.now().toString())).build()
}
