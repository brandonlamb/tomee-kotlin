package net.brandonlamb.api.common.webapi.models

import javax.xml.bind.annotation.XmlAccessOrder
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorOrder
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name = "ping")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlAccessorOrder(XmlAccessOrder.ALPHABETICAL)
data class Ping(var timeStamp: String? = null, var message: String? = null)
