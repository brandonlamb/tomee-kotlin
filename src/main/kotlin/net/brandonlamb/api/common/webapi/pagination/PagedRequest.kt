package net.brandonlamb.api.common.webapi.pagination

import net.brandonlamb.api.common.configuration.mappers.PagedRequestConfig
import javax.annotation.PostConstruct
import javax.enterprise.context.RequestScoped
import javax.inject.Inject
import javax.servlet.http.HttpServletRequest
import javax.ws.rs.core.Context

@RequestScoped
open class PagedRequest @Inject constructor(
  val config: PagedRequestConfig,
  @Context var request: HttpServletRequest
) {
  var limit: Int = 0
  var offset: Int = 0

  @PostConstruct
  fun postConstruct() {
    var parm = request.getParameter("limit")
    limit = if (parm != null && parm.length > 0) parm.toInt() else config.getLimit()

    parm = request.getParameter("offset")
    offset = if (parm != null && parm.length > 0) parm.toInt() else config.getOffset()

//    limit = Optional.ofNullable(httpServletRequest!!.getParameter("limit")).map<Int>(Function<String, Int> { Integer.valueOf(it) }).orElse(pagedRequestConfig!!.getLimit())
//    if (limit <= 0 || limit > pagedRequestConfig!!.getLimitMax()) {
//      throw BadRequestException(100003, "Invalid limit query parameter")
//    }
//
//    offset = Optional.ofNullable(httpServletRequest.getParameter("offset")).map<Int>(Function<String, Int> { Integer.valueOf(it) }).orElse(pagedRequestConfig!!.getOffset())
//    if (offset < 0) {
//      throw BadRequestException(100004, "Invalid offset query parameter")
//    }
  }
}
