package net.brandonlamb.api.common.validation.validators;

import com.sbux.gred.common.validation.constraints.Required;
import com.sbux.gred.common.webapi.exceptions.RequiredParameterException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RequiredValidator implements ConstraintValidator<Required, Object> {

  private String propertyName;

  @Override
  public void initialize(final Required constraintAnnotation) {
    propertyName = constraintAnnotation.value();
  }

  @Override
  public boolean isValid(final Object value, final ConstraintValidatorContext context) {
    if (value != null) {
      return true;

    }
    throw new RequiredParameterException(propertyName);
  }
}
