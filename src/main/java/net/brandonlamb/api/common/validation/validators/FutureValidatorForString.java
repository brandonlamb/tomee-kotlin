package net.brandonlamb.api.common.validation.validators;

import com.sbux.gred.common.converters.DateConverter;
import com.sbux.gred.common.validation.constraints.Future;
import com.sbux.gred.common.webapi.exceptions.ValidationException;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FutureValidatorForString implements ConstraintValidator<Future, String> {

  @Override
  public void initialize(final Future constraintAnnotation) {
  }

  @Override
  public boolean isValid(final String date, final ConstraintValidatorContext context) {
    try {
      final LocalDate value = DateConverter.toLocalDate(date);
      return value == null || value.isAfter(LocalDate.now());
    } catch (final ValidationException e) {
      return false;
    }
  }
}
