package net.brandonlamb.api.common.validation.constraints;

import com.sbux.gred.common.validation.validators.RequiredValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Marker annotation
 */
@Target({PARAMETER})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = RequiredValidator.class)
public @interface Required {

  String value();

  String message() default "{com.sbux.gred.common.validation.constraints.Required.message}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
