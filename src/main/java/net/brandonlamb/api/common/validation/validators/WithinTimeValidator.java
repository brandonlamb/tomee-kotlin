package net.brandonlamb.api.common.validation.validators;

import com.sbux.gred.common.validation.constraints.WithinTime;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * This validation annotation only works on ChronoUnit of DAYS WEEKS MONTHS YEARS DECADES CENTURIES
 * MILLENNIA ERAS. This will throw exception for other units.
 */
public class WithinTimeValidator implements ConstraintValidator<WithinTime, LocalDate> {

  private ChronoUnit unit;
  private int max;

  @Override
  public void initialize(WithinTime constraintAnnotation) {
    unit = constraintAnnotation.unit();
    max = constraintAnnotation.value();
  }

  @Override
  public boolean isValid(final LocalDate value, ConstraintValidatorContext context) {
    if (value == null) {
      return true;
    }
    return Math.abs(value.until(LocalDate.now(), unit)) <= max;
  }
}
