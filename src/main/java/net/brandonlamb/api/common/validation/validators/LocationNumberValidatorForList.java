package net.brandonlamb.api.common.validation.validators;

import com.sbux.gred.common.validation.constraints.LocationNumber;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LocationNumberValidatorForList
    implements ConstraintValidator<LocationNumber, List<Integer>> {

  private boolean required;

  @Override
  public void initialize(LocationNumber constraintAnnotation) {
    required = constraintAnnotation.required();
  }

  @Override
  public boolean isValid(final List<Integer> value, ConstraintValidatorContext context) {
    if (value == null) {
      return !required;
    } else if (value.isEmpty()) {
      return !required;
    } else {
      return value.stream().allMatch(location -> location > 0 && location < 100000);
    }
  }
}
