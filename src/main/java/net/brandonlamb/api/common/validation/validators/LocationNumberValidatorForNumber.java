package net.brandonlamb.api.common.validation.validators;

import com.sbux.gred.common.validation.constraints.LocationNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LocationNumberValidatorForNumber
    implements ConstraintValidator<LocationNumber, Number> {

  private boolean required;

  @Override
  public void initialize(LocationNumber constraintAnnotation) {
    required = constraintAnnotation.required();
  }

  @Override
  public boolean isValid(final Number value, ConstraintValidatorContext context) {
    return value == null ? !required : value.longValue() < 100000 && value.longValue() > 0;
  }
}
