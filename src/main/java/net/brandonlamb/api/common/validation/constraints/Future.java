package net.brandonlamb.api.common.validation.constraints;

import com.sbux.gred.common.validation.validators.FutureValidatorForLocalDate;
import com.sbux.gred.common.validation.validators.FutureValidatorForString;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Marker annotation
 */
@Target({PARAMETER, FIELD, METHOD})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {
    FutureValidatorForLocalDate.class,
    FutureValidatorForString.class
})
public @interface Future {

  String message() default "{com.sbux.gred.common.validation.constraints.Future.message}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
