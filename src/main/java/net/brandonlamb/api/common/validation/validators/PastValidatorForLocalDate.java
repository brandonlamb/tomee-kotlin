package net.brandonlamb.api.common.validation.validators;

import com.sbux.gred.common.validation.constraints.Future;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PastValidatorForLocalDate implements ConstraintValidator<Future, LocalDate> {

  @Override
  public void initialize(final Future constraintAnnotation) {
  }

  @Override
  public boolean isValid(final LocalDate date, final ConstraintValidatorContext context) {
    return date == null || date.isBefore(LocalDate.now());
  }
}
