package net.brandonlamb.api.common.validation.constraints;

import com.sbux.gred.common.validation.validators.LocationNumberValidatorForList;
import com.sbux.gred.common.validation.validators.LocationNumberValidatorForNumber;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(
    validatedBy = {LocationNumberValidatorForNumber.class,
                   LocationNumberValidatorForList.class}
)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface LocationNumber {

  boolean required() default false;

  String message() default ("Invalid location number");

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
