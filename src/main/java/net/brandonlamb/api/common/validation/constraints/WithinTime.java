package net.brandonlamb.api.common.validation.constraints;

import com.sbux.gred.common.validation.validators.WithinTimeValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.temporal.ChronoUnit;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = {WithinTimeValidator.class})
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface WithinTime {

  String message() default ("Invalid location number");

  int value();

  ChronoUnit unit() default ChronoUnit.YEARS;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
